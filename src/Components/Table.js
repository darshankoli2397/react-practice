import React from 'react';
//const PureRenderMixin = require('react/addons').addons.PureRenderMixin;
//import Immutable from 'immutable';

export default class Table extends React.Component {

  constructor() {
    this.state = {
      data: Immutable.List(),
      filteredData: Immutable.List(),
    };
    this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);

  }

  componentWillMount() {
    this.setState({
      data: Immutable.fromJS(this.props.data).toList(),
      filteredData: Immutable.fromJS(this.props.data).toList()
    });
  }

  filterData(event) {
    event.preventDefault();
    const regex = new RegExp(event.target.value, 'i');
    const filtered = this.state.data.filter(function(datum) {
      return (datum.get('title').search(regex) > -1);
    });

    this.setState({
      filteredData: filtered,
    });
  }

  render() {
    const { filteredData } = this.state;
    const prettyRows = filteredData.map(function(datum) {
      return (
        <tr>
          <td>{ datum.get("id") }</td>
          <td>{ datum.get("title") }</td>
        </tr>
      );
    });


    return(
      <div className="Table container">
        <input
          type="text"
          className="form-control"
          onChange={ this.filterData.bind(this) }
          placeholder="Search" />

        <table className="table">
          <thead>
            <th>ID</th>
            <th>Title</th>
          </thead>

          <tbody>
            { prettyRows }
          </tbody>
       </table>
     </div>);
   }
}
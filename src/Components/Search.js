import React, { Component } from "react";
import axios from "axios";

class Search extends Component {
    state = { name: "" };

    handleChange = (e) => this.setState({ name: e.target.value });

    handleSubmit = (e) => {
        e.preventDefault();

        const user = this.state.name;

        axios.post("https://www.drupal.org/search/user/", { user }).then((res) => {
            console.log(res);
            console.log(res.data);
        });
    };

    render() {
        return ( <
            >
            <
            div >
            <
            h1 > Search here < /h1> <
            form align = "center" >
            <
            input type = "search"
            onChange = { this.handleChange }
            value = { this.state.value }
            placeholder = "Search" /
            >
            <
            button onClick = {
                (e) => this.handleSubmit(e)
            } > Search < /button> < /
            form > <
            /div> < / >
        );
    }
}

export default Search;
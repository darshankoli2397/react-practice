import React,{ Component } from "react"

import Axios from "axios"

class UserList extends Component {
    constructor(prop) {
        super(prop);
        this.onChangeUserName = this.onChangeUserName.bind(this);

        this.onSubmit = this.onSubmit.bind(this);

        this.state = { users: null, profile:"", name:"" }
    }

    onChangeUserName(e) {
      this.setState({ name: e.target.value })
  }

  onSubmit(e) {
    e.preventDefault()

    const userObject = {
        name: this.state.name,

    };
    Axios.get(`https://www.drupal.org/search/user/${this.state.name}`).then(
      response => {
          if (response.status === 200) {
              // all is good
              console.log('all is good');
              console.log(response);
              var olList = response.data.match(/(?<=\<ol class="search-results user-results"\>\s+).*?(?=\s+\<\/ol)/gs);
              
              var links = response.data.match(
                /(\b(https?):\/\/[-A-Z0-9+&@#\\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi

              );
              //preg_match_all('|https?://(?:www\.)?twitter.com/#!/[a-z0-9_]+|im', $text, $matched)
              
              this.setState({ users: olList });
              this.setState({ profile: links });
            } else {
                console.log('something is wrong');
                // something is wrong
            }
        }
    );

  }
   
    render() {
        return ( <React.Fragment>

<
            h1 > Search Here users of Drupal < /h1>  <
            form align = "center"
            onSubmit = { this.onSubmit } >
            <
            input type = "search"
            name = "name"
            onChange = { this.onChangeUserName }
            placeholder = "Search"
            required / >
            <
            button type = "submit" > Search < /button >   < /
            form >

          <h3>Relevent Search Results For Your Keywords : </h3>



          <table border="2" height="100" width="300">
          <tr>
            <td align="center"><h2>Username</h2></td> 
            <td><h2>Profile Links</h2></td> 

          </tr>

          <tr>
            
            <td dangerouslySetInnerHTML = { 
                { __html: this.state.users }
            } />

            <td align="char" dangerouslySetInnerHTML = {
                { __html: this.state.profile }
            }
            />
          </tr>
          </table>
                 
        </React.Fragment>
        );
    }
}

export default UserList;